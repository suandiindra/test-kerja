"use strict";
var express = require('express');
var router = express.Router();
var validator = require('../services/validator');
var transaksisController = require('../controllers/Transaksis');

var service = 'transaksis';

// get transaksis or search transaksis
router.get('/'+service, transaksisController.find);

// get transaksi
router.get('/'+service+'/:id', transaksisController.findOne);

// To add validation, add a middlewave like the below. Works for just POST calls only
// function(req,res,next){
//     req._required = [ // _required should contain all the fails that are required
//     'name',
//     'name2'
//     ];

//     next();
// }, validator,

// create transaksi(s) a single transaksi object will create one transaksi while an array of transaksis will create multiple transaksis
router.post('/'+service,transaksisController.create);

// update all records that matches the query
router.put('/'+service, transaksisController.update);

// update a single record
router.patch('/'+service+'/:id', transaksisController.updateOne);

// delete all records that matches the query
router.delete('/'+service, transaksisController.delete);

// Delete a single record
router.delete('/'+service+'/:id', transaksisController.deleteOne);

// restore a previously deleted record
router.post('/'+service+'/:id/restore', transaksisController.restore);

module.exports = router;

"use strict";
var express = require('express');
var router = express.Router();
var validator = require('../services/validator');
var clisController = require('../controllers/Clis');

var service = 'cli';

// get clis or search clis
router.get('/'+service, clisController.getdata);

module.exports = router;

"use strict";
var express = require('express');
var router = express.Router();
var validator = require('../services/validator');
var produksController = require('../controllers/Produks');

var service = 'produks';

// get produks or search produks
router.get('/'+service, produksController.find);

// get produk
router.get('/'+service+'/:id', produksController.findOne);

// To add validation, add a middlewave like the below. Works for just POST calls only
// function(req,res,next){
//     req._required = [ // _required should contain all the fails that are required
//     'name',
//     'name2'
//     ];

//     next();
// }, validator,

// create produk(s) a single produk object will create one produk while an array of produks will create multiple produks
router.post('/'+service, produksController.create);

// update all records that matches the query
router.put('/'+service, produksController.update);

// update a single record
router.patch('/'+service+'/:id', produksController.updateOne);

// delete all records that matches the query
router.delete('/'+service, produksController.delete);

// Delete a single record
router.delete('/'+service+'/:id', produksController.deleteOne);

// restore a previously deleted record
router.post('/'+service+'/:id/restore', produksController.restore);

module.exports = router;

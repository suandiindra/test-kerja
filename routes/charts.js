"use strict";
var express = require('express');
var router = express.Router();
var validator = require('../services/validator');
var chartsController = require('../controllers/Charts');

var service = 'charts';

// get charts or search charts
router.get('/'+service, chartsController.find);

// get chart
router.get('/'+service+'/:id', chartsController.findOne);

// To add validation, add a middlewave like the below. Works for just POST calls only
// function(req,res,next){
//     req._required = [ // _required should contain all the fails that are required
//     'name',
//     'name2'
//     ];

//     next();
// }, validator,

// create chart(s) a single chart object will create one chart while an array of charts will create multiple charts
router.post('/'+service, chartsController.create);

// update all records that matches the query
router.put('/'+service, chartsController.update);

// update a single record
router.patch('/'+service+'/:id', chartsController.updateOne);

// delete all records that matches the query
router.delete('/'+service, chartsController.delete);

// Delete a single record
router.delete('/'+service+'/:id', chartsController.deleteOne);

// restore a previously deleted record
router.post('/'+service+'/:id/restore', chartsController.restore);

module.exports = router;

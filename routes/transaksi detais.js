"use strict";
var express = require('express');
var router = express.Router();
var validator = require('../services/validator');
var transaksi_detaisController = require('../controllers/Transaksi_detais');

var service = 'transaksi detais';

// get transaksi detais or search transaksi detais
router.get('/'+service, transaksi_detaisController.find);

// get transaksi detai
router.get('/'+service+'/:id', transaksi_detaisController.findOne);

// To add validation, add a middlewave like the below. Works for just POST calls only
// function(req,res,next){
//     req._required = [ // _required should contain all the fails that are required
//     'name',
//     'name2'
//     ];

//     next();
// }, validator,

// create transaksi detai(s) a single transaksi detai object will create one transaksi detai while an array of transaksi detais will create multiple transaksi detais
router.post('/'+service, transaksi_detaisController.create);

// update all records that matches the query
router.put('/'+service, transaksi_detaisController.update);

// update a single record
router.patch('/'+service+'/:id', transaksi_detaisController.updateOne);

// delete all records that matches the query
router.delete('/'+service, transaksi_detaisController.delete);

// Delete a single record
router.delete('/'+service+'/:id', transaksi_detaisController.deleteOne);

// restore a previously deleted record
router.post('/'+service+'/:id/restore', transaksi_detaisController.restore);

module.exports = router;

'use strict';

var chai = require('chai');
chai.should();
var config = require('../../config');
var chaiAsPromised = require('chai-as-promised');
// chai.use(chaiAsPromised);
var mongooseMock = require('mongoose-mock');
// var expect = chai.expect;
var sinon = require('sinon');
var q = require('q');
var sinonChai = require('sinon-chai');
chai.use(sinonChai);
var Transaksi;
// Testing The Transaksi Model
describe('Transaksi Model',function(){

    var id;
    var id2;

    before(function(){  /* jslint ignore:line */
        Transaksi = require('../../models/Transaksis');
        var workers = require('../../services/queue/workers');
        var workers1 = require('../../services/queue/workers');
        var workers2 = require('../../services/queue/workers');
        var workers3 = require('../../services/queue/workers');
    });

    describe('Transaksi CRUDS', function(){
        it('should save data', function(done){
            var mytransaksi = Transaksi.create({name: 'femi'});

            mytransaksi.then(function(res){
                res.should.be.an.object; /* jslint ignore:line */
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should read data', function(done){
            var mytransaksi = Transaksi.findOne({where: {name: 'femi'}});

            mytransaksi.then(function(res){
                res.should.be.an.object; /* jslint ignore:line */
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should read all data', function(done){
            var mytransaksi = Transaksi.findAll();

            mytransaksi.then(function(res){
                res.should.be.an.array; /* jslint ignore:line */
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should update data', function(done){
            var cb = sinon.spy();
            var mytransaksi = Transaksi.update({name: 'Olaoluwa'}, { where: {name: 'femi'}});

            mytransaksi.then(function(res){
                cb();
                cb.should.have.been.calledOnce; /* jslint ignore:line */
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should search data', function(done){
            // Search needs more work for more accuracy
            var mytransaksi = Transaksi.search('femi');

            mytransaksi.then(function(res){
                res.should.be.an.object; /* jslint ignore:line */
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should delete data', function(done){
            var cb2 = sinon.spy();
            var ourtransaksi = Transaksi.bulkCreate([{name:'Olaolu'},{name: 'fola'},{name: 'bolu'}]);

            ourtransaksi.then(function(res){
                return Transaksi.destroy({where: {name: 'bolu'}});
            }).then(function(res){
                cb2();
                cb2.should.have.been.calledOnce; /* jslint ignore:line */
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should add createdAt', function(done){
            var mytransaksi = Transaksi.create({name: 'this is for the gods'});

            mytransaksi.then(function(res){
                id = res._id;
                res.should.have.property('createdAt');
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should add updatedAt', function(done){
            var mytransaksi = Transaksi.create({name: 'i am a demigod!'});
            mytransaksi.then(function(res){
                id2 = res._id;
                return Transaksi.update({name: 'This is the titan'}, {where: {_id: id}});
            })
            .then(function(res){
                return Transaksi.findOne({where: {name: 'This is the titan'}});
            })
            .then(function(res){
                res.should.have.property('updatedAt');
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should tag database entries properly', async function(){
            var mytransaksi = await Transaksi.create({name: 'femi',someOtherStringData: 'stuff'});
            
            return q.Promise(function(resolve, reject) {
            setTimeout(function(){
                Transaksi.findOne({where: {_id: mytransaksi._id}})
                .then(function(res){
                    if(typeof res.tags === 'string'){
                        var _tags = res.tags.split(', ');
                        _tags.length.should.equal(2);/* jslint ignore:line */
                    }else{
                        res.tags.length.should.equal(2);/* jslint ignore:line */
                    }
                    
                    resolve(res);
                })
                .catch(function(err){
                    reject(err);
                });
            },3000);
            });
            });

        it('should count returned records', function(done){
            var mytransaksi = Transaksi.count({where: {name: 'This is the titan'}});

            mytransaksi.then(function(res){
                res.should.be.a.number; /* jslint ignore:line */
                done();
            })
            .catch(function(err){
                done(err);
            });
        });

        it('should find a record by id and delete', function(done){
            var mytransaksi = Transaksi.findOne({where: {_id: id2}});

            var transaksier = mytransaksi.then(function(res){
                return res.destroy();
            })
            .then(function(res){
                res.should.be.an('object');
                done();
            });
            
        });

        it('should have transaction object', function(done){
            var mytransaksi = Transaksi.transaction.should.exist;  /* jslint ignore:line */
            done();
        });

    });
});

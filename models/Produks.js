"use strict";

var db = require('../services/database').sql;
var queue = require('../services/queue');

var table = 'Produks';
var debug = require('debug')(table);

// Define schema
var schemaObject = {
    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
    nama_produk: {
        type: db._sequelize.STRING
    },
    stok: {
        type: db._sequelize.INTEGER
    },
    price: {
        type: db._sequelize.FLOAT
    }
    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
};


schemaObject.owner = {
    type: db._sequelize.STRING
};

schemaObject.createdBy = {
    type: db._sequelize.STRING
};

schemaObject.client = {
    type: db._sequelize.STRING
};

schemaObject.developer = {
    type: db._sequelize.STRING
};

schemaObject.tags = {
    type: db._sequelize.STRING
};

schemaObject._id = {
    type: db._sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
};


// Define the table
var Produks = db.define(table, schemaObject, {
    // Don't really delete data
    // paranoid: true,
    // define indexes here
    indexes:[{
        fields:['tags']
    },
    {
        unique: true,
        fields:['_id']
    }]
});

Produks.associate = function (models) {
    // models.Produks.belongsTo(models.toPop, { foreignKey: 'toPop', sourceKey: '_id' });
};

// Produks.hasMany(Produks, {foreignKey: 'toPop', sourceKey: '_id'});

// Adding hooks
Produks.afterCreate(function(user, options) {
    // Indexing for search
    var ourDoc = user.dataValues;
    ourDoc.isSQL = true;
    ourDoc.model = table;

    // Dump it in the queue
    queue.create('searchIndex', ourDoc)
    .save();
});

Produks.search = function(string){
    return Produks.findAll({
        where: {
            tags: {
                $like: '%'+string
            }
        }
    });
};

Produks.sync();

Produks.transaction = db.transaction;

module.exports = Produks;
// ToDo: Test transactions

"use strict";

var db = require('../services/database').sql;
var queue = require('../services/queue');
var table = 'Transaksi';
var debug = require('debug')(table);

// Define schema
var schemaObject = {
    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
    transaksi_id: {
        type: db._sequelize.STRING,
        primaryKey : true
    },
    id_keranjang: {
        type: db._sequelize.STRING
    },
    tgl_transaksi: {
        type: db._sequelize.DATE
    },
    amount: {
        type: db._sequelize.FLOAT
    },
    point: {
        type: db._sequelize.INTEGER
    },
    qty: {
        type: db._sequelize.INTEGER
    },
    kasir_id: {
        type: db._sequelize.STRING
    }

    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
};


// schemaObject.owner = {
//     type: db._sequelize.STRING
// };

schemaObject.createdBy = {
    type: db._sequelize.STRING
};

// schemaObject.client = {
//     type: db._sequelize.STRING
// };

// schemaObject.developer = {
//     type: db._sequelize.STRING
// };

// schemaObject.tags = {
//     type: db._sequelize.STRING
// };

// schemaObject._id = {
//     type: db._sequelize.INTEGER,
//     primaryKey: true,
//     autoIncrement: true
// };


// Define the table
var Transaksis = db.define(table, schemaObject, {
    // Don't really delete data
    // paranoid: true,
    // define indexes here
    indexes:[
    {
        unique: true,
        fields:['transaksi_id']
    }]
});

Transaksis.associate = function (models) {
    // models.Transaksis.belongsTo(models.toPop, { foreignKey: 'toPop', sourceKey: '_id' });
};

// Transaksis.hasMany(Transaksis, {foreignKey: 'toPop', sourceKey: '_id'});

// Adding hooks
Transaksis.afterCreate(function(user, options) {
    // Indexing for search
    var ourDoc = user.dataValues;
    ourDoc.isSQL = true;
    ourDoc.model = table;

    // Dump it in the queue
    queue.create('searchIndex', ourDoc)
    .save();
});

Transaksis.search = function(string){
    return Transaksis.findAll({
        where: {
            tags: {
                $like: '%'+string
            }
        }
    });
};

Transaksis.sync();

Transaksis.transaction = db.transaction;

module.exports = Transaksis;
// ToDo: Test transactions

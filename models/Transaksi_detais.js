"use strict";

var db = require('../services/database').sql;
var queue = require('../services/queue');

var table = 'Transaksi_detais';
var debug = require('debug')(table);

// Define schema
var schemaObject = {
    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
    transaksi_detail_id: {
        type: db._sequelize.UUID,
        defaultValue: db._sequelize.UUIDV4,
        primaryKey : true
    },
    transaksi_id: {
        type: db._sequelize.STRING,
    },
    m_produk_id: {
        type: db._sequelize.STRING
    },
    qty: {
        type: db._sequelize.INTEGER
    },
    price: {
        type: db._sequelize.FLOAT
    }
    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
};


schemaObject.createdBy = {
    type: db._sequelize.STRING
};


// Define the table
var Transaksi_detais = db.define(table, schemaObject, {
    // Don't really delete data
    // paranoid: true,
    // define indexes here
    indexes:[
    {
        unique: true,
        fields:['transaksi_detail_id']
    }]
});

Transaksi_detais.associate = function (models) {
    // models.Transaksi_detais.belongsTo(models.toPop, { foreignKey: 'toPop', sourceKey: '_id' });
};

// Transaksi_detais.hasMany(Transaksi_detais, {foreignKey: 'toPop', sourceKey: '_id'});

// Adding hooks
Transaksi_detais.afterCreate(function(user, options) {
    // Indexing for search
    var ourDoc = user.dataValues;
    ourDoc.isSQL = true;
    ourDoc.model = table;

    // Dump it in the queue
    queue.create('searchIndex', ourDoc)
    .save();
});

Transaksi_detais.search = function(string){
    return Transaksi_detais.findAll({
        where: {
            tags: {
                $like: '%'+string
            }
        }
    });
};

Transaksi_detais.sync();

Transaksi_detais.transaction = db.transaction;

module.exports = Transaksi_detais;
// ToDo: Test transactions

"use strict";

var db = require('../services/database').sql;
var queue = require('../services/queue');

var table = 'Charts';
var debug = require('debug')(table);

// Define schema
var schemaObject = {
    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
    chart_id: {
        type: db._sequelize.UUID,
        defaultValue: db._sequelize.UUIDV4,
        primaryKey : true
    },
    m_user_id: {
        type: db._sequelize.STRING
    },
    tgl_masuk_chart: {
        type: db._sequelize.DATE
    },
    tgl_checkout: {
        type: db._sequelize.DATE
    },
    m_produk_id: {
        type: db._sequelize.STRING
    },
    qty: {
        type: db._sequelize.INTEGER
    },
    price: {
        type: db._sequelize.FLOAT
    },
    isactive: {
        type: db._sequelize.STRING
    }
    // ++++++++++++++ Modify to your own schema ++++++++++++++++++
};


schemaObject.owner = {
    type: db._sequelize.STRING
};

schemaObject.createdBy = {
    type: db._sequelize.STRING
};


schemaObject.tags = {
    type: db._sequelize.STRING
};

// schemaObject._id = {
//     type: db._sequelize.INTEGER,
//     autoIncrement: true
// };


// Define the table
var Charts = db.define(table, schemaObject, {
    // Don't really delete data
    // paranoid: true,
    // define indexes here
    indexes:[{
        fields:['tags']
    }]
});

Charts.associate = function (models) {
    // models.Charts.belongsTo(models.toPop, { foreignKey: 'toPop', sourceKey: '_id' });
};

// Charts.hasMany(Charts, {foreignKey: 'toPop', sourceKey: '_id'});

// Adding hooks
Charts.afterCreate(function(user, options) {
    // Indexing for search
    var ourDoc = user.dataValues;
    ourDoc.isSQL = true;
    ourDoc.model = table;

    // Dump it in the queue
    queue.create('searchIndex', ourDoc)
    .save();
});

Charts.search = function(string){
    return Charts.findAll({
        where: {
            tags: {
                $like: '%'+string
            }
        }
    });
};

Charts.sync();

Charts.transaction = db.transaction;

module.exports = Charts;
// ToDo: Test transactions

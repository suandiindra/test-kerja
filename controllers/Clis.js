"use strict";

var Clis = require('../models').Clis;
var Trash = require('../models').Trash;
var q = require('q');
var queue = require('../services/queue');
var debug = require('debug')('clisController');
var _ = require('lodash');
const fs = require('fs');
const clifunction = require('./clifunction');
var service = 'Clis';

var ClisController = {};

ClisController.getdata = function(req,res,next){
    let datatransaksi = fs.readFileSync('dummy_data/datatransaksi.json', 'utf-8');
    let dataorder = fs.readFileSync('dummy_data/dataorder.json', 'utf-8');
    datatransaksi = JSON.parse(datatransaksi)
    dataorder = JSON.parse(dataorder)
    let result = clifunction.countTotalMountTransactionAndPoint(datatransaksi,dataorder)

    res.ok(result)
};

module.exports = ClisController;

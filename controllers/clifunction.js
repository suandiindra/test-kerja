"use strict";
var express = require('express');
const token_hardcoded = "1010123"

module.exports = {
    countTotalMountTransactionAndPoint : function (data1,data2){
        // cek data1 
        let amountTransaksi = 0;
        let totalPoint = 0;
        let qtyItem = 0;
        if(data1){
            for(let i = 0; i<data1.length; i++){
                amountTransaksi = amountTransaksi + data1[i].total_amount_transaction;
                let point = Math.floor(data1[i].total_amount_transaction/10000);
                
                let trans = data1[i].total_amount_transaction
                // console.log(point,trans);
                if(trans < 1000000){
                    point = point * 1
                }else if( trans > 1000000 && trans < 10000000){
                    point = point * 1.05
                }else if(trans > 10000000 && trans < 20000000){
                    point = point * 1.1
                }else if(trans > 20000000 && trans < 30000000){
                    point = point * 1.2
                }else if(trans > 30000000 && trans < 40000000){
                    point = point * 1.3
                }else if(trans > 40000000){
                    point = point * 1.4
                }
                totalPoint = totalPoint + point
                // cari user order
                let userorder = data1[i].user
                var newDataFilter = data2.filter(function (el) {
                    return el.user = userorder
                });
                let orders = newDataFilter.length
                console.log(newDataFilter,orders);
                for(let x = 0; x< orders; x++){
                    console.log(newDataFilter[x].products);
                    let dtprod = newDataFilter[x].products
                    for(let z = 0 ; z< dtprod.length; z++){
                        qtyItem = qtyItem + dtprod[z].qty
                    }
                }
            }
            
        }
        console.log(amountTransaksi,totalPoint,qtyItem);
        let result = {
            totalAmountTransaction : amountTransaksi,
            totalPoints : totalPoint,
            totalItems : qtyItem
        }
        return result;
    }
}
